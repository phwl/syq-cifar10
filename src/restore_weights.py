from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import math
import time
import os
import numpy as np
import tensorflow as tf

import cifar10

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string('eval_data', 'test',
                           """Either 'test' or 'train_eval'.""")
tf.app.flags.DEFINE_string('checkpoint_dir', './syq_train',
                           """Directory where to read model checkpoints.""")
tf.app.flags.DEFINE_integer('eval_interval_secs', 60 * 5,
                            """How often to run the eval.""")
tf.app.flags.DEFINE_integer('num_examples', 10000,
                            """Number of examples to run.""")
tf.app.flags.DEFINE_boolean('run_once', False,
                         """Whether to run eval only once.""")

def restore_model():

    with tf.Session() as sess:

        ckpt = tf.train.get_checkpoint_state(FLAGS.checkpoint_dir)

        if ckpt and ckpt.model_checkpoint_path:
            # Restores from checkpoint
            saver = tf.train.import_meta_graph('./syq_train/model.ckpt-100000.meta')
            saver.restore(sess, ckpt.model_checkpoint_path)
            # Assuming model_checkpoint_path looks something like:
            #   /my-favorite-path/cifar10_train/model.ckpt-0,
            # extract global_step from it.
            # print(sess.run(tf.trainable_variables()))
            pathset = './trained_weights'
            if not (os.path.exists(pathset)):
                # create the directory you want to save to
                os.mkdir(pathset)
            variables_names = [v.name for v in tf.trainable_variables()]
            values = sess.run(variables_names)
            a = {}
            for k, v in zip(variables_names, values):
                print("Variable: ", k)

                print("Shape: ", v.shape)

                print(v)

                k = k.replace("/", "_")

                a[k] = v

            np.save('weight.npy', a)

        else:
            print('No checkpoint file found')
            return


if __name__ == '__main__':
    restore_model()
    a = np.load('weight.npy').item()
    print(a.keys())

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Julian Faraone 2018
# Quantization method based on paper: SYQ: Learning Symmetric Quantization For Efficient Deep Neural Networks
#
# Scalars are initialized to 1.0 in this example, however in paper they are initialized to the mean of the initial floating point weights they are applied to
# More info about the paper here:
# https://arxiv.org/abs/1807.00301
# And code:
# https://github.com/julianfaraone/SYQ


import tensorflow as tf 
import numpy as np

import functools

from tensorflow.python.ops import variable_scope

G = tf.get_default_graph()

_origin_get_variable = tf.get_variable
_object_stack = []


def _new_get_variable(*args, **kwargs):
    v = _origin_get_variable(*args, **kwargs)
    if len(_object_stack) != 0:
        return _object_stack[-1]._fn(v)
    else:
        return v


class TFVariableReplaceHelper(object):

    def __init__(self, fn):
        self._old_get_variable = None
        self._fn = fn

    def __enter__(self):
        global _object_stack
        _object_stack.append(self)
        self._old_get_variable = tf.get_variable
        tf.get_variable = _new_get_variable
        variable_scope.get_variable = _new_get_variable

    def __exit__(self, *args):
        global _object_stack
        _object_stack.pop()
        tf.get_variable = self._old_get_variable
        variable_scope.get_variable = self._old_get_variable


def replace_variable(fn):
    return TFVariableReplaceHelper(fn)

#SYQ quantization for binary or ternary weights
def syq_quantization(x, eta, name, value, binary=True):
    
    shape = x.get_shape()

    eta_x = tf.stop_gradient(tf.reduce_max(tf.abs(x)) * eta)

    list_of_masks = []

    #CONV Layers
    if 'conv' in name:

        w_s = _origin_get_variable('Ws', [(shape[0].value*shape[1].value),1], collections=[tf.GraphKeys.GLOBAL_VARIABLES, 'SCALING'], initializer=tf.constant_initializer(value[name]))
        
        #each pixel
        for i in range(shape[0].value):
            for j in range(shape[1].value):
                ws = w_s[(shape[1].value*i) + j, 0]
                mask = tf.ones(shape)
                mask_p = tf.where(x[i,j,:,:] > eta_x, mask[i,j,:,:] * ws, mask[i,j,:,:])
                mask_np = tf.where(x[i,j,:,:] < -eta_x, mask[i,j,:,:] * ws, mask_p)
                list_of_masks.append(mask_np)
                
        masker = tf.stack(list_of_masks)
        masker = tf.reshape(masker, [i.value for i in shape])

        if binary:
            mask_z = tf.ones(shape)
        else:
            mask_z = tf.where((x < eta_x) & (x > - eta_x), tf.zeros(shape), tf.ones(shape))

        with G.gradient_override_map({"Sign": "Identity", "Mul": "Add"}):
            w =  tf.sign(x) * tf.stop_gradient(mask_z)

        w = w * masker

    #FC Layers
    else:

        wn = _origin_get_variable('Wn', collections=[tf.GraphKeys.GLOBAL_VARIABLES, 'scale_fc'], initializer=value[name])

        mask = tf.ones(shape)
        mask_p = tf.where(x > eta_x, tf.ones(shape) * wn, mask)
        mask_np = tf.where(x < -eta_x, tf.ones(shape) * wn, mask_p)
        
        if binary:
            mask_z = tf.ones(shape)
        else:
            mask_z = tf.where((x < eta_x) & (x > - eta_x), tf.zeros(shape), tf.ones(shape))

        with G.gradient_override_map({"Sign": "Identity", "Mul": "Add"}):
            w =  tf.sign(x) * tf.stop_gradient(mask_z)

        w = w * mask_np

    return w


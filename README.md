CIFAR-10 benchmark using SYQ

---

CIFAR-10 is a common benchmark in machine learning for image recognition.

http://www.cs.toronto.edu/~kriz/cifar.html

Code in this directory demonstrates how to use SYQ to train and evaluate a convolutional neural network (CNN) on both CPU and GPU using CIFAR-10. For more details see our paper:

Title:   SYQ: Learning Symmetric Quantization For Efficient Deep Neural Networks
Authors: Faraone, Julian; Fraser, Nicholas; Blott, Michaela; Leong, Philip H. W.
Publication: eprint arXiv:1807.00301
Comment:	
Published as a conference paper at the 2018 IEEE Conference on Computer Vision and Pattern Recognition (CVPR)
Bibliographic Code: 2018arXiv180700301F
Url: https://uk.arxiv.org/abs/1807.00301
